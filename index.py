from flask import Flask, render_template, request
from werkzeug import secure_filename
import cv2
import numpy as np
import time
from moviepy.editor import VideoFileClip, concatenate_videoclips
import os
import face_recognition
import random

app = Flask(__name__)

@app.route('/')
def upload_file1():
   return render_template('index.html')
saved_video_file_name = ""
@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
	html  = """
<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="static/img/favicon.png" type="image/png">
	<title>Intruder Detector</title>
	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="static/css/bootstrap.css">
	<link rel="stylesheet" href="static/vendors/linericon/style.css">
	<link rel="stylesheet" href="static/css/font-awesome.min.css">
	<link rel="stylesheet" href="static/vendors/owl-carousel/owl.carousel.min.css">
	<link rel="stylesheet" href="static/css/magnific-popup.css">
	<link rel="stylesheet" href="static/vendors/nice-select/css/nice-select.css">
	<link rel="stylesheet" href="static/vendors/animate-css/animate.css">
	<link rel="stylesheet" href="static/vendors/flaticon/flaticon.css">
	<!-- main css -->
	<link rel="stylesheet" href="static/css/style.css">
</head>

<body>

	<!--================Header Menu Area =================-->
	<header class="header_area">
		<div class="main_menu">
			<nav class="navbar navbar-expand-lg navbar-light">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<a class="navbar-brand" href="index.html"><img src="static/img/logo.png" alt=""></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
					 aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
					</div>
				</div>
			</nav>
		</div>
	</header>
	<!--================Header Menu Area =================-->

	<!--================Home Banner Area =================-->
	<section class="banner-area owl-carousel" id="home">
		<div class="single_slide_banner slide_bg1">
			<div class="container">
				<div class="row fullscreen d-flex align-items-center">
					<div class="banner-content col-lg-12 justify-content-center">
						<h3>AUTOMATIC INTRUDER DETECTOR</h3>
						<h4>Process Successfull. Output will be found in the Output Folder</h4>
						<br><a href='/done'> <button>To view Video Click here</button></a> &nbsp;&nbsp;&nbsp;<a href='/'> <button>GO back</button></a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--================End Home Banner Area =================-->


	<!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<script src="static/js/jquery-3.2.1.min.js"></script>
	<script src="static/js/popper.js"></script>
	<script src="static/js/bootstrap.min.js"></script>
	<script src="static/js/stellar.js"></script>
	<script src="static/js/jquery.magnific-popup.min.js"></script>
	<script src="static/vendors/nice-select/js/jquery.nice-select.min.js"></script>
	<script src="static/vendors/isotope/imagesloaded.pkgd.min.js"></script>
	<script src="static/vendors/isotope/isotope-min.js"></script>
	<script src="static/vendors/owl-carousel/owl.carousel.min.js"></script>
	<script src="static/js/jquery.ajaxchimp.min.js"></script>
	<script src="static/js/mail-script.js"></script>
	<!--gmaps Js-->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
	<script src="static/js/gmaps.min.js"></script>
	<script src="static/js/theme.js"></script>
</body>

</html>
	"""	
	if request.method != 'POST':
		return "Error try the correct URL"
	if request.method == 'POST':
		f = request.files['file']
		f.save(secure_filename(f.filename))
		a = time.time()  
		# Video Capture 
		# capture = cv2.VideoCapture(0)
		image = face_recognition.load_image_file(f.filename)
		face_encoding = face_recognition.face_encodings(image)[0]
		known_faces = [face_encoding]
		print(face_encoding)
		print("___________________________________________________________")
		video_name_list = ['vid.mp4','vid3.mp4']
		for k in video_name_list:
		    face_locations = []
		    face_encodings = []
		    face_names = []
		    video_file_name = k
		    capture = cv2.VideoCapture(video_file_name)
		    fps = capture.get(cv2.CAP_PROP_FPS)
		    # History, Threshold, DetectShadows 
		    # fgbg = cv2.createBackgroundSubtractorMOG2(50, 200, True)
		    fgbg = cv2.createBackgroundSubtractorMOG2(300, 400, True)

		    # Keeps track of what frame we're on
		    frameCount = 0
		    checker = 0
		    tym = []
		    face_found = 0
		    def tymer(frameCount,fps):
		        return (frameCount/fps)
		    while(1):
		        # Return Value and the current frame
		        ret, frame = capture.read()
		        
		        #  Check if a current frame actually exist
		        if not ret:
		            if checker == 1:
		                print("end")
		                #tym_list.append((str(now.second) +"p"+ str(now.microsecond)))
		                tym_list.append(tymer(frameCount,fps))
		                tym.append(tym_list)
		                del tym_list
		            break
		        cv2.imshow('Mask', frame)
		        frameCount += 1
		        # Resize the frame
		        resizedFrame = cv2.resize(frame, (0, 0), fx=0.50, fy=0.50)

		        # Get the foreground mask
		        fgmask = fgbg.apply(resizedFrame)
		        cv2.imshow('Mask', fgmask)
		        # Count all the non zero pixels within the mask
		        count = np.count_nonzero(fgmask)
		        #print('Frame: %d, Pixel Count: %d' % (frameCount, count))

		        # Determine how many pixels do you want to detect to be considered "movement"
		        # if (frameCount > 1 and count > 5000):
		        if (frameCount > 1 and count > 100):
		            if checker == 0:
		                checker = 1
		                tym_list = []
		                tym_list.append(tymer(frameCount,fps))
		                print("start")    
		                #cv2.putText(resizedFrame, 'Someones stealing your honey', (10, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 2, cv2.LINE_AA)
		                #cv2.imwrite("frame%d.jpg" % frameCount, frame)
		            if checker == 1:
		 # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
		                rgb_frame = frame[:, :, ::-1]
		                # Find all the faces and face encodings in the current frame of video
		                face_locations = face_recognition.face_locations(rgb_frame)
		                face_encodings = face_recognition.face_encodings(rgb_frame, face_locations)
		                for face_encoding in face_encodings:
		                    # See if the face is a match for the known face(s)
		                    match = face_recognition.compare_faces(known_faces, face_encoding, tolerance=0.50)
		                    if match[0]:
		                        print("face found")
		                        face_found = 1
		                print("searching face")
		        else:
		            if checker == 1:
		                checker = 0
		                print ("End")
		                if face_found == 0:
		                    del tym_list
		                else:
		                    face_found = 1
		                    print("face successfully detected")
		                    face_found = 0    
		                    #tym_list.append((str(now.second) +"p"+ str(now.microsecond)))
		                    tym_list.append(tymer(frameCount,fps))
		                    tym.append(tym_list)
		                    del tym_list                    
		        #cv2.imshow('Frame', resizedFrame)
		        #cv2.imshow('Mask', frame)
		         
		        #print("time stamp current frame:",frameCount/fps)
		        k = cv2.waitKey(1) & 0xff
		        if k == 27:
		            break

		    capture.release()
		    cv2.destroyAllWindows()
		    print(tym)
		    count = 0
		    filenames_objects = []
		    file_name = []
		    filename = ""
		    for i in tym:
		        count = count + 1
		        filename = "filename"+str(count)+".mp4"
		        file_name.append(filename)
		        clip = VideoFileClip(video_file_name).subclip(i[0],i[1])
		        clip.write_videofile(filename)    
		        clip = VideoFileClip(filename)
		        filenames_objects.append(clip)
		    if len(filenames_objects) >0:
		        final_clip = concatenate_videoclips(filenames_objects)
		        global saved_video_file_name
		        saved_video_file_name = "my_concatenation"+str(random.choice([1,2,3,4,5,6,7,8,9,10,11,12,23,34,45,46,57,67,78,89,90]))+".mp4"
		        final_clip.write_videofile("static/"+saved_video_file_name)

		    print (time.time() - a, "Tym Took")
		    if len(file_name) > 0:
		        for i in file_name:
		        	print(i)
		        	print("_______________________")
		        	os.remove(i)
		        print(i," Removed")
	        return html
@app.route('/done')
def done():
	global saved_video_file_name
	return app.send_static_file(saved_video_file_name)
if __name__ == '__main__':
   app.run(debug = True)
